package util;


/**
 * Write a description of class Nodo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Nodo<T> 
{
    // instance variables - replace the example below with your own
    
    private T info;
    private Nodo<T> sig;
    
     protected Nodo()
    {
             
    }
    
     protected Nodo(T info, Nodo<T> sig)
    {
             this.info=info;
             this.sig=sig;
             
    }
    
     protected T getInfo()
    {
        return this.info;
    }
    
     protected void setInfo(T info)
    {
       this.info=info;
    }
    
    
     protected Nodo<T> getSig()
    {
        return this.sig;
    }
    
    protected void setSig(Nodo<T> sig)
    {
       this.sig=sig;
    }
    

}
