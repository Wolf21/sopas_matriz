package util;


/**
 * Write a description of class ListaS here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ListaS<T>
{
    // instance variables - replace the example below with your own
    
    
    private Nodo<T> cabeza;
    private int tamanio=0;
    
    public ListaS()
    {
        this.cabeza=null;
        
    }

    
    
    public void insertarInicio(T info)
    {
        
            this.cabeza=new Nodo<T>(info, this.cabeza);
            this.tamanio++; 
    }
    
    
    public void insertarFinal(T info)
    {
        if(this.esVacio())
            this.insertarInicio(info);
        else
        {
            
            try{
            
                Nodo<T> ult=this.getPos(this.getTamanio()-1);
                ult.setSig(new Nodo<T>(info,null));
                this.tamanio++;
                
            
            }catch(Exception e)
            {
                System.err.println(e.getMessage());
            }
               
        }
    }
    
    
    private Nodo<T> getPos(int i) throws Exception
    {
        
       if(this.esVacio() || i<0 || i>=this.tamanio)
           throw new Exception("Error en índices");
      
    Nodo<T> aux=this.cabeza;
    
    while(i>0)
        {
            aux=aux.getSig();
            i--;
        }
    
    return aux;
    }
    
    
    
    public T get(int pos)
    {
    
        try{
            
                Nodo<T> x=this.getPos(pos);
                
                return(x.getInfo());
            
            }catch(Exception e)
            {
                System.err.println(e.getMessage());
                return null;
            }
    }
    
    
    
    public void set(int pos, T infoNuevo)
    {
    
        try{
            
                Nodo<T> x=this.getPos(pos);
                x.setInfo(infoNuevo);
               
            
            }catch(Exception e)
            {
                System.err.println(e.getMessage());
               
            }
    }
    
    
    public void insertarCopia(int datoBuscar, int repetir)
    {
        /*
            NO SE PUEDE UTILIZAR NINGUN METODO DE LOS CREADOS
            
            l=<3,4,5,6,6,2>
            l.inserCopia(6,3);
            l=<3,4,5,6,6,6,6,6,6,6,6,2>
            */
        if (tamanio>=1){
            Nodo<T> aux= this.cabeza;
            int i= tamanio;
        
        while(i-->0){
          if(aux.getInfo().equals(datoBuscar)){
                int a=repetir;
                Nodo<T> aux2=aux.getSig();
            while(a-->0){
                aux.setSig(new Nodo (datoBuscar,null));
                System.out.println(aux.getInfo().toString());
                aux=aux.getSig();
            }
            aux.setSig(aux2);
            aux=aux.getSig();
          }
          else  aux=aux.getSig();
        }
       }
    }
    
    
    
    
    public boolean esVacio()
    {
        return (this.cabeza==null);
    }
    
    
    public int getTamanio()
    {
        return this.tamanio;
    }
    
    public String toString()
    {
        Nodo<T> aux=this.cabeza;
        String msg="";
        while (aux!=null)
        {
            msg+=aux.getInfo().toString()+",";
            aux=aux.getSig();
        }
       
    return msg;
    }
}
