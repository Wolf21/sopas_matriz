package Negocio;
import java.io.FileNotFoundException;
import util.ListaS;
import java.io.IOException;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author TitaWolf
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }/* // LAS LIBRERÍAS NO CARGABAAN PARA EL PDF 
//     /*CREACIÓN DEL */
//    public void crearPDF() throws FileNotFoundException, IOException {
//        String dest = "D:\\Documents\\NetBeansProjects";
//
//        
//        PdfWriter writer = new PdfWriter(dest);
//        PdfDocument pdf = new PdfDocument(writer);
//
//        Document document = new Document(pdf, PageSize.A4);
//        document.setMargins(35, 25, 30, 25);
//        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
//
//        Table table = new Table(sopas[0].length);
//        crearTabla(font, table);
//        table.setWidth(100);
//
//        document.add(table);
//        document.add(new Paragraph(rts));
//        document.close();
//        mostrarPdf(dest);
//    }
//
//    public void mostrarPdf(String ruta) {
//
//        try {
//            String exe = "rundll32 url.dll,FileProtocolHandler " + ruta;
//
//            Process p = Runtime.getRuntime().exec(exe);
//            p.waitFor();
//        } catch (InterruptedException ex) {
//            Logger.getLogger(SopaDeLetras.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(SopaDeLetras.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    private void crearTabla(PdfFont font, Table tabla) {
//
//        for (int i = 0; i < this.sopas.length; i++) {
//            for (int j = 0; j < sopas[i].length; j++) {
//                if (s[i][j] == true) {
//                    tabla.addCell(String.valueOf(sopas[i][j])).setFont(font);
//                    tabla.getCell(i, j).setBackgroundColor(Color.GREENYELLOW);
//                    tabla.getCell(i, j).setFontColor(Color.LIGHT_GRAY);
//
//                } else {
//                    tabla.addCell(String.valueOf(sopas[i][j])).setFont(font).setBackgroundColor(Color.LIGHT_GRAY);
//                }
//            }
//    }
//    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    public SopaDeLetras(char[][] m) {
        this.sopas = m;
    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esDispersa() {
        int filas = this.sopas.length;
        boolean esDispersa = false;
        for (int i = 0; i < sopas.length; i++) {
            if (filas != this.sopas[i].length) {
                return esDispersa = true;
            }
        }
        return esDispersa;
    }

    public boolean esCuadrada() {
        return (!esDispersa() && this.sopas.length == this.sopas[0].length);
    }

    public boolean esRectangular() {
        return (!esDispersa() && !esCuadrada() || this.sopas.length < this.sopas[0].length || this.sopas.length > this.sopas[0].length);
    }

    /*
    retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) {
        char[] pal = palabra.toCharArray();
        for (int i = 0; i < sopas.length; i++) {
            if (pal.length <= sopas.length) {

            }
        }

        return 0;
    }

   private int cuantasLetras(char []palabra){
       int esta=0;
       for (int i = 0; i < sopas.length; i++) {
           for (int j = 0; j < sopas[i].length; j++) {
               if(sopas[i][j]==palabra[0])
                   esta++;
           }
       }
       return esta;
}

    public int[][] buscarTodasPosiciones(char palabra[]) {
        int t = cuantasLetras(palabra);
        int[][] posiciones=new int [t][2];
        int k=0;
        for (int i = 0; i < sopas.length || k<t; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if(sopas[i][j]==palabra[0]){
            posiciones[k][0]=i;
            posiciones[k][1]=j;
                k++;}
            }
            
        }
    return posiciones;
    }
    
    
    

//    public String toStringPosiciones(String palabra) {
//        String msg = "";
//        int aux = 0;
//        char[] pala = palabra.toCharArray();
//        int[][] posiciones = buscarTodasPosiciones(pala);
//        for (int i = 0; i < posiciones.length; i++) {
//            for (int j = 0; j < posiciones[i].length; j++) {
//                aux = j;
//            }
//            msg += posiciones[i][aux];
//            msg += "\n";
//
//        }
//        return msg;
//    }

    public boolean horizontalDerecha(char []palabra, int x, int y) {
        
        int filas = sopas[0].length;
        int aux = 0;
        if (y + palabra.length > filas)//comprobar de que la palabra sea de menor tamaño que la fila que se va a recorrer
        {
            return false;
        }

        for (int j = y; j < filas && aux < palabra.length; j++) {
            if (palabra[aux] != sopas[x][j]) {
                return false;
            }
            aux++;
        }
        return true;
    }

    public boolean horizontalIzquierda(char []palabra, int x, int y) {
        int filas = sopas[0].length;
        int aux = 0;
        if ((y+1) - palabra.length <0)//comprobar de que la palabra sea de menor tamaño que la fila que se va a recorrer
        {
            return false;
        }

        for (int j = y; j > 0 && aux < palabra.length; j--) {
            if (palabra[aux] != sopas[x][j]) {
                return false;
            }
            aux++;
        }
        return true;

    }

    public boolean verticalArriba(char[] palabra, int x, int y) {
        int columnas = sopas.length;
        int aux = 0;
        if (palabra.length > columnas || (x+1) - palabra.length  <0)//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }

        for (int j = x; j > 0 && aux < palabra.length; j--) {
            if (palabra[aux] != sopas[j][y]) {
                return false;
            }
            aux++;
        }
        return true;
 /*
        for()
        while(palabra.length
        */
    }

    public boolean verticalAbajo(char []palabra, int x, int y) {
        int columnas = sopas.length;
        int aux = 0;
        if (palabra.length>columnas || x + palabra.length > columnas  )//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }

        for (int j = x; j < columnas && aux < palabra.length; j++) {
            // sopas.length-x > palabra.length
            if (palabra[aux] != sopas[j][y]) {
                return false;
            }
            aux++;
        }
        return true;
    }

    public boolean diagonalArribaAbajo1(char []palabra, int x, int y) {
        int columnas = sopas.length;
        int filas = sopas[0].length;
        int i = x;
        int j = y;

        if (palabra.length + x  >filas || palabra.length + y> columnas)//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }
        for (int k = 0; k < palabra.length; k++) {
            if (sopas[i][j] != palabra[k]) {
                return false;
            }
            i++;
            j++;
        }
        return true;
    }

    public boolean diagonalArribaAbajo2(char []palabra, int x, int y) {
        int columnas = sopas.length;
        int filas = sopas[0].length;

        int i = x;
        int j = y;

        if ((y+1) - palabra.length <0 || palabra.length>columnas || x + palabra.length > filas )//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }
        for (int k = 0; k < palabra.length; k++) {
            if (sopas[i][j] != palabra[k]) {
                return false;
            }

            i++;
            j--;
        }
        return true;
    }

    public boolean diagonalAbajoArriba1(char []palabra, int x, int y) {
        int columnas = sopas.length;
        int filas = sopas[0].length;

        int i = x;
        int j = y;

        if (y + palabra.length > filas || palabra.length > columnas || (x+1) - palabra.length  <0)//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }
        for (int k = 0; k < palabra.length; k++) {
            if (sopas[i][j] != palabra[k]) {
                return false;
            }

            i--;
            j++;
        }
        return true;
    }

    public boolean diagonalAbajoArriba2(char []palabra, int x, int y) {
        int columnas = sopas.length;
        int filas = sopas[0].length;

        int i = x;
        int j = y;

        if ((y+1) - palabra.length <0 || palabra.length > columnas || (x+1) - palabra.length  <0)//comprobar de que la palabra sea de menor tamaño que la columna que se va a recorrer
        {
            return false;
        }
        for (int k = 0; k < palabra.length; k++) {
            if (sopas[i][j] != palabra[k]) {
                return false;
            }

            i--;
            j--;
        }
        return true;
    }

    private char[] getDiagonalPrincipal() throws Exception {
        if (!esCuadrada()) {
            System.err.println("La matriz no es cuadrada");
        }
        char diagonal[] = new char[sopas.length];
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (i == j) {
                    diagonal[i] = sopas[i][j];
                }
            }
        }
        return diagonal;

    }
    
  
    public String mensajeHorizontalDerecha(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(horizontalDerecha(palabra, s[i][0], s[i][1]))
                    msg+="Horizontal hacia la derecha. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";
        }
        return msg;
    }
    
    public String mensajeHorizontalIzquierda(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(horizontalIzquierda(palabra, s[i][0], s[i][1]))
                    msg+="Horizontal hacia la izquierda. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    
    public String mensajeVerticalAbajo(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(verticalAbajo(palabra, s[i][0], s[i][1]))
                    msg+="Vertical hacia abajo. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    public String mensajeVerticalArriba(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(verticalArriba(palabra, s[i][0], s[i][1]))
                    msg+="Vertical hacia arriba. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    public String mensajeDBD(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(diagonalArribaAbajo1(palabra, s[i][0], s[i][1]))
                    msg+="Diagonal izquierda a derecha bajando. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    public String mensajeDBI(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(diagonalArribaAbajo2(palabra, s[i][0], s[i][1]))
                    msg+="Diagonal izquierda a derecha subiendo. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    
    public String mensajeDAD(char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(diagonalAbajoArriba1(palabra, s[i][0], s[i][1]))
                    msg+="Diagonal derecha a izquierda bajando. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
    public String mensajeDAI (char []palabra){
        String msg="";
        int [][] s=buscarTodasPosiciones(palabra);
        for (int i = 0; i < s.length; i++) {
                if(diagonalAbajoArriba2(palabra, s[i][0], s[i][1]))
                    msg+="Diagonal derecha a izquierda subiendo. Fila: "+ s[i][0]+ "\t"+ "Columna: "+s[i][1] + "\n";          
        }
        return msg;
    }
   
	
}



