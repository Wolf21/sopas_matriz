/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.*;
/**

 */
public class Matriz_Excel_Ejemplo {
     
    String ruta;
    
    public Matriz_Excel_Ejemplo(String ruta){
        this.ruta=ruta;
    }
    
    public char[][] leerExcel() throws Exception {
         
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(this.ruta));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        //System.out.println("Filas:"+canFilas);
        char[][] m=new char[canFilas][];
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            m[i]=new char[cantCol];
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            m[i][j]=filas.getCell(j).getStringCellValue().charAt(0);
            //System.out.print(valor);
        }
        //me lo separa con comas al finalizar la fila
       }
        return m;
    }
}
